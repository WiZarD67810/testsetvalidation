using AppConsole.Buisness.Exceptions;
using AppConsole.Buisness.Models;
using Xunit;

namespace Tests
{
    public class TestCapital
    {
        private static readonly int CAPITAL_AMOUNT = 50000;
        [Fact]
        public void TestCapitalThrowException()
        {
            InvalidCapitalException invalidCapitalException = Assert.Throws<InvalidCapitalException>(() => new Capital(10000));
            Assert.Equal("Capital value is less than the minimal capital", invalidCapitalException.Message);
        }

        [Fact]
        public void TestCapitalImplicit()
        {
            Capital capital = CAPITAL_AMOUNT;
            Assert.Equal(CAPITAL_AMOUNT, (int)capital);
        }
    }
}
