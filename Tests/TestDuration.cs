using AppConsole;
using AppConsole.Buisness.Exceptions;
using Xunit;

namespace Tests
{
    public class TestDuration
    {
        private static readonly int DURATION_VALUE = 7;
        private static readonly int NUMBER_MONTH_OF_THE_YEAR = 12;

        [Theory]
        [InlineData(7, 84)]
        [InlineData(20, 240)]
        [InlineData(25, 300)]
        public void TestDurationYears(int duration, int result)
        {
            Duration dur = new(duration);
            Assert.Equal(result, dur.DurationOfTheRepayment);
        }

        [Fact]
        public void TestDurationThrowException()
        {
            InvalidDurationException invalidDurationException1 = Assert.Throws<InvalidDurationException>(() => new Duration(5));
            Assert.Equal("Loan duration is less than the minimal duration", invalidDurationException1.Message);

            InvalidDurationException invalidDurationException2 = Assert.Throws<InvalidDurationException>(() => new Duration(50));
            Assert.Equal("Loan duration is more than the maximal duration", invalidDurationException2.Message);
        }

        [Fact]
        public void TestDurationImplicit()
        {
            Duration duration = DURATION_VALUE;
            Assert.Equal(DURATION_VALUE * NUMBER_MONTH_OF_THE_YEAR, (int)duration);
        }
    }
}
