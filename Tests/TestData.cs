using AppConsole;
using AppConsole.Buisness.Enums;
using AppConsole.Buisness.Models;
using AppConsole.DAL;
using System;
using Xunit;

namespace Tests
{
    public class TestData
    {
        DataLoader dataLoader = new HardLoader();

        [Theory]
        [InlineData(0.62, ELoan.GOOD, 7)]
        [InlineData(0.73, ELoan.VERY_GOOD, 15)]
        [InlineData(0.55, ELoan.VERY_GOOD, 10)]
        public void TestDataNominalRate(double expected, ELoan eLoan, int duration)
        {
            NominalRate nominalRate = expected;
            Assert.Equal((double)nominalRate, dataLoader.getNominalRate(eLoan, new Duration(duration)));
        }
    }
}
