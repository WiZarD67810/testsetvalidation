using AppConsole;
using AppConsole.Buisness.Enums;
using AppConsole.Buisness.Models;
using AppConsole.DAL;
using System;
using Xunit;

namespace Tests
{
    public class TestLoan
    {
        private Loan loan;

        public TestLoan()
        {
            //TODO: les huit lignes du dessous dans une méthode
            Capital capital = 175000;
            Duration duration = 25;
            Guarantee guarantee = new Guarantee();
            ELoan eLoan = ELoan.GOOD;

            guarantee.DefineRateForASmoker();
            guarantee.DefineRateForACardiac();
            guarantee.DefineRateForAEngineer();

            loan = new Loan(capital, duration, guarantee, eLoan, new NominalRate(new HardLoader().getNominalRate(eLoan, duration)));
        }

        [Fact]
        public void TestLoanMonthlyInstalment()
        {
            Assert.Equal(681.14, loan.GetMonthlyInstalment());
        }

        [Fact]
        public void TestLoanAmountOfGuaranteeMonthly()
        {
            Assert.Equal(102.08, loan.AmountOfGuaranteeMonthly());
        }

        [Fact]
        public void TestLoanMonthlyInstalmentWithGuarantee()
        {
            Assert.Equal(783.22, loan.GetMonthlyInstalmentWithGuarantee());
        }

        [Fact]
        public void TestLoanAmountOfLoan()
        {
            Assert.Equal(59964.46, loan.AmountOfLoan());
        }

        [Fact]
        public void TestLoanAmountOfInterest()
        {
            Assert.Equal(29340.46, loan.AmountOfInterest());
        }

        [Fact]
        public void TestLoanAmountOfGuarantee()
        {
            Assert.Equal(30624.00, loan.AmountOfGuarantee());
        }

        [Fact]
        public void TestLoanAmountOfTheCapitalRepayWithDuration()
        {
            Assert.InRange<double>(loan.AmountOfTheCapitalRepayWithDuration(new Duration(10)), 63420.2, 63420.3);
        }
    }
}
