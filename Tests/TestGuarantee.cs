﻿using AppConsole;
using System;
using Xunit;

namespace Tests
{
    public class TestGuarantee
    {
        private Guarantee guarantee;

        public TestGuarantee()
        {
            guarantee = new Guarantee();
        }

        [Fact]
        public void TestGuaranteeBaseRate()
        {
            Assert.Equal(0.3, guarantee);
        }

        [Fact]
        public void TestGuaranteeSportman()
        {
            guarantee.DefineRateForASportman();

            Assert.Equal(0.25, guarantee);
        }

        [Fact]
        public void TestGuaranteeEngineer()
        {
            guarantee.DefineRateForAEngineer();

            Assert.Equal(0.25, guarantee);
        }

        [Fact]
        public void TestGuaranteePilot()
        {
            guarantee.DefineRateForAPilot();

            Assert.Equal(0.45, guarantee);
        }

        [Fact]
        public void TestGuaranteeCardiac()
        {
            guarantee.DefineRateForACardiac();

            Assert.Equal(0.60, guarantee);
        }

        [Fact]
        public void TestGuarenteeCardiacAndSportmanAndSmoker()
        {
            guarantee.DefineRateForACardiac();
            guarantee.DefineRateForASportman();
            guarantee.DefineRateForASmoker();

            Assert.Equal(0.70, guarantee);
        }
    }
}
