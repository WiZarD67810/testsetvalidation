﻿using AppConsole.Buisness.Enums;
using AppConsole.Buisness.Models;
using AppConsole.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConsole
{
    public class Calculator
    {
        private static Calculator _instance;

        private Calculator() { }

        public static Calculator GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Calculator();
            }
            return _instance;
        }

        public void CreateLoan()
        {
            Capital capitalOfTheLoan = createCapitalForTheLoan();
            Duration durationOfTheLoan = createDurationForTheLoan();
            ELoan eLoan = createTypeForTheLoan();
            Guarantee guaranteeOfTheLoan = createGuaranteeForTheLoan();

            Loan loan = new Loan(capitalOfTheLoan, durationOfTheLoan, guaranteeOfTheLoan, eLoan, new NominalRate(new HardLoader().getNominalRate(eLoan, durationOfTheLoan)));
            Console.WriteLine(loan.GetInfoOfTheLoan());
        }

        #region Capital
        private Capital createCapitalForTheLoan()
        {
            Capital capital;

            Console.WriteLine("What's the amoung do you want ? \nGrasp a value upper 50000.");
            
            try
            {
                capital = readTheConsoleAndConvertToInt();
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                capital = createCapitalForTheLoan();
            }

            return capital;
        }
        #endregion

        #region Duration
        private Duration createDurationForTheLoan()
        {
            Duration duration;

            Console.WriteLine("What duration do you want ? \nGrasp value between : 7 and 25 years");

            try
            {
                duration = readTheConsoleAndConvertToInt();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                duration = createDurationForTheLoan();
            }

            return duration;
        }
        #endregion

        #region Type
        private ELoan createTypeForTheLoan()
        {
            Console.WriteLine("What's the type of loan you want ?");

            return convertTheConsoleReadToTypeLoanEnum();
        }

        private ELoan convertTheConsoleReadToTypeLoanEnum()
        {
            string valueOfTheTypeLoan = "";

            do
            {
                Console.WriteLine("Grasp type in these posibility : good, very good or excellent type of loan");
                valueOfTheTypeLoan = readTheConsole();
            } while (checkValidityFromTheConsoleReadForTheTypeLoan(valueOfTheTypeLoan));

            return convertStringToTypeLoan(valueOfTheTypeLoan);
        }

        private bool checkValidityFromTheConsoleReadForTheTypeLoan(string value)
        {
            return !(transformStringToLowerCase(value).Equals("good") ||
                   transformStringToLowerCase(value).Equals("very good") ||
                   transformStringToLowerCase(value).Equals("excellent"));
        }

        private string transformStringToLowerCase(string value)
        {
            return value.ToLower();
        }

        private ELoan convertStringToTypeLoan(string stringLoan)
        {
            if (stringLoan == "good")
            {
                return ELoan.GOOD;
            }
            else if (stringLoan == "very good")
            {
                return ELoan.VERY_GOOD;
            }
            else
            {
                return ELoan.EXCELLENT;
            }
        }

        #endregion

        #region Guarantee
        private Guarantee createGuaranteeForTheLoan()
        {
            Console.WriteLine("Please reply at these questions per yes or no.");

            Guarantee guaranteeOfTheLoan = new Guarantee();

            if (convertTheConsoleReadForTheGuarantee("Are you a sportman ?"))
            {
                guaranteeOfTheLoan.DefineRateForASportman();
            }
            if (convertTheConsoleReadForTheGuarantee("Are you a smoker ?"))
            {
                guaranteeOfTheLoan.DefineRateForASmoker();
            }
            if (convertTheConsoleReadForTheGuarantee("Are you a affected by cardiac disorder ?"))
            {
                guaranteeOfTheLoan.DefineRateForACardiac();
            }
            if (convertTheConsoleReadForTheGuarantee("Are you a IT Engineer ?"))
            {
                guaranteeOfTheLoan.DefineRateForAEngineer();
            }
            if (convertTheConsoleReadForTheGuarantee("Are you a fighter pilot ?"))
            {
                guaranteeOfTheLoan.DefineRateForAPilot();
            }

            return guaranteeOfTheLoan;
        }

        private bool convertTheConsoleReadForTheGuarantee(string question)
        {
            string responseToTheQuestion = "";
            do
            {
                Console.WriteLine(question);
                responseToTheQuestion = readTheConsole();
            } while (checkValidityOfTheGuarantee(responseToTheQuestion));

            return responseToTheQuestion.Equals("yes");
        }

        private bool checkValidityOfTheGuarantee(string value)
        {
            return !(transformStringToLowerCase(value).Equals("yes") || transformStringToLowerCase(value).Equals("no"));
        }
        #endregion

        private int readTheConsoleAndConvertToInt()
        {
            int valueOfTheParse = 0;
            try
            {
                valueOfTheParse = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception exceptionParse)
            {
                Console.WriteLine("[ERROR] : Value no valid");
            }

            return valueOfTheParse;
        }

        private string readTheConsole()
        {
            return Console.ReadLine();
        }
    }
}
