﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConsole.Buisness.Data
{
    public class ModelRateDTO
    {
        private const int NUMBER_OF_MONTH_IN_A_YEAR = 12;
        public string TypeLoan { get; }
        public int Duration { get; }
        public double Rate { get; }

        public ModelRateDTO(string type, int duration, double rate)
        {
            TypeLoan = type;
            Duration = duration * NUMBER_OF_MONTH_IN_A_YEAR;
            Rate = rate;
        }
    }
}
