﻿using AppConsole.Buisness.Exceptions;
using System;

namespace AppConsole
{
    public class Duration
    {
        private static readonly int DURATION_MIN = 7;
        private static readonly int DURATION_MAX = 25;

        private static readonly int NUMBER_OF_MONTH_IN_A_YEAR = 12;
        public int DurationOfTheRepayment { get; }

        public Duration(int years)
        {
            if(years < DURATION_MIN)
            {
                throw new InvalidDurationException("Loan duration is less than the minimal duration");
            }
            if(years > DURATION_MAX)
            {
                throw new InvalidDurationException("Loan duration is more than the maximal duration");
            }    

            DurationOfTheRepayment = years * NUMBER_OF_MONTH_IN_A_YEAR;
        }

        public static implicit operator Duration(int years) => new(years);
        public static implicit operator int(Duration duration) => duration.DurationOfTheRepayment;
    }
}