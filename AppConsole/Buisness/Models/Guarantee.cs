﻿using System;

namespace AppConsole
{
    public class Guarantee
    {
        private static readonly double SPORTSMAN = 0.05;
        private static readonly double SMOKER = 0.15;
        private static readonly double CARDIAC = 0.3;
        private static readonly double ENGINEER = 0.05;
        private static readonly double PILOT = 0.15;

        private double _BASE_RATE_OF_THE_GUARANTEE = 0.30;
        public double RateOfTheGuarantee { get; private set; }

        public Guarantee()
        {
            RateOfTheGuarantee = _BASE_RATE_OF_THE_GUARANTEE;
        }

        public static implicit operator double(Guarantee guarantee) => guarantee.RateOfTheGuarantee;

        public void DefineRateForASportman()
        {
            RateOfTheGuarantee = Math.Round(RateOfTheGuarantee -= SPORTSMAN, 2);
        }

        public void DefineRateForAEngineer()
        {
            RateOfTheGuarantee = Math.Round(RateOfTheGuarantee -= ENGINEER, 2);
        }

        public void DefineRateForAPilot()
        {
            RateOfTheGuarantee = Math.Round(RateOfTheGuarantee += PILOT, 2);
        }

        public void DefineRateForACardiac()
        {
            RateOfTheGuarantee = Math.Round(RateOfTheGuarantee += CARDIAC, 2);
        }

        public void DefineRateForASmoker()
        {
            RateOfTheGuarantee = Math.Round(RateOfTheGuarantee += SMOKER, 2);
        }
    }
}