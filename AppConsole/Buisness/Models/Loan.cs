using AppConsole.Buisness.Enums;
using AppConsole.Buisness.Models;
using AppConsole.DAL;
using System;
using System.Collections.Generic;

namespace AppConsole
{
    public class Loan
    {
        private static readonly int NUMBER_OF_MONTH_IN_A_YEAR = 12;
        public Capital Capital { get; private set; }
        public Duration Duration { get; private set; }
        public Guarantee Guarantee { get; private set; }
        public ELoan ELoan { get; private set; }
        public NominalRate NominalRate { get; private set; }


        public Loan(Capital capital, Duration duration, Guarantee guarantee, ELoan eLoan, NominalRate nominalRate)
        {
            Capital = capital;
            Duration = duration;
            Guarantee = guarantee;
            ELoan = eLoan;
            NominalRate = nominalRate;
        }

        public string GetInfoOfTheLoan()
        {
            return "\n\nLoan : " +
                "\n - Capital : " + Capital.Amount +
                "\n - Duration : " + Duration.DurationOfTheRepayment + " (in Months) - " + Duration.DurationOfTheRepayment / NUMBER_OF_MONTH_IN_A_YEAR + " (in years)" +
                "\n - Type of loan : " + ELoan.ToString() +
                "\n - Guarantee : " + Guarantee.RateOfTheGuarantee +
                "\n - Nominal rate : " + NominalRate.RateOfTheLoan +
                "\n\n - Monthly instalment : " + GetMonthlyInstalment() + " (without guarantee)" +
                "\n - Monthly instalment : " + GetMonthlyInstalmentWithGuarantee() + " (with guarantee)" +
                "\n - Amount of the guarantee monthly : " + AmountOfGuaranteeMonthly() +
                "\n - Amount of the loan : " + AmountOfLoan() +
                "\n - Amount of instalment : " + AmountOfInterest() +
                "\n - Amount of the guarantee : " + AmountOfGuarantee() +
                "\n - Amount repay after 10 years : " + AmountOfTheCapitalRepayWithDuration(new Duration(10));

        }

        public double GetMonthlyInstalment()
        {
            double resultCapitalRate = Capital * rateMonthlyOfInterest();
            double resultSecondPart = 1 - Math.Pow((1 + rateMonthlyOfInterest()), - Duration);
            return Math.Round(resultCapitalRate / resultSecondPart, 2);
        }

        public double GetMonthlyInstalmentWithGuarantee()
        {
            return Math.Round(GetMonthlyInstalment() + AmountOfGuaranteeMonthly(), 2);
        }

        public double AmountOfGuaranteeMonthly()
        {
            double amountOfGuarantee = (Capital * (Guarantee / 100)) / NUMBER_OF_MONTH_IN_A_YEAR;
            return Math.Round(amountOfGuarantee, 2);
        }

        public double AmountOfTheCapitalRepayWithDuration(Duration duration)
        {
            double capitalRemained = Capital;

            for (int indiceOfTheMonth = 1; indiceOfTheMonth <= duration; indiceOfTheMonth++)
            {
                double interest = monthlyInterestOfTheRemainingCapital(capitalRemained);
                capitalRemained = capitalRemained - (GetMonthlyInstalment() - interest);
            }

            return Math.Round(Capital - capitalRemained, 2);
        }

        public double AmountOfLoan()
        {
            return Math.Round(AmountOfInterest() + AmountOfGuarantee(), 2);
        }

        public double AmountOfGuarantee()
        {
            return Math.Round(AmountOfGuaranteeMonthly() * Duration, 2);
        }

        public double AmountOfInterest()
        {
            double capitalRemained = Capital;
            double globalInterest = 0;

            for (int indiceOfTheMonth = 1; indiceOfTheMonth <= Duration; indiceOfTheMonth++)
            {
                double interest = monthlyInterestOfTheRemainingCapital(capitalRemained);
                capitalRemained = capitalRemained - (GetMonthlyInstalment() - interest);
                globalInterest += interest;
            }

            return Math.Round(globalInterest, 2);
        }

        private double monthlyInterestOfTheRemainingCapital(double remainingCapital)
        {
            double rateAtMonth = rateMonthlyOfInterest();
            return remainingCapital * rateAtMonth;
        }

        private double rateMonthlyOfInterest()
        {
            return ((NominalRate / 100) / NUMBER_OF_MONTH_IN_A_YEAR);
        }
    }
}
