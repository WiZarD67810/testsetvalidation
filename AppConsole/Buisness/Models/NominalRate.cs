namespace AppConsole.Buisness.Models
{
    public class NominalRate
    {
        public double RateOfTheLoan { get; private set; }

        public NominalRate(double rate)
        {
            RateOfTheLoan = rate;
        }

        public static implicit operator NominalRate(double rate) => new(rate);
        public static implicit operator double(NominalRate nominalRate) => nominalRate.RateOfTheLoan;
    }
}
