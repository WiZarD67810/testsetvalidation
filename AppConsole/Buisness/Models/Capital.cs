﻿using AppConsole.Buisness.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConsole.Buisness.Models
{
    public class Capital
    {
        private static readonly int CAPITAL_MIN = 50000;
        public int Amount { get; }
        
        public Capital(int amount)
        {
            if(amount < CAPITAL_MIN)
            {
                throw new InvalidCapitalException("Capital value is less than the minimal capital");
            }

            Amount = amount;
        }

        public static implicit operator Capital(int value) => new(value);
        public static implicit operator int(Capital capital) => capital.Amount;
    }
}
