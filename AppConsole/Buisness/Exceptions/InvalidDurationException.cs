﻿using System;

namespace AppConsole.Buisness.Exceptions
{
    public class InvalidDurationException: Exception
    {
        public InvalidDurationException() { }
        public InvalidDurationException(string message) : base(message) { }
        
    }
}
