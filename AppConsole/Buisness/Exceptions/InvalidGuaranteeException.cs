﻿using System;

namespace AppConsole.Buisness.Exceptions
{
    public class InvalidGuaranteeException: Exception
    {
        public InvalidGuaranteeException() { }
        public InvalidGuaranteeException(string message) : base(message) { }
    }
}
