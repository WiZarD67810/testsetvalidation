﻿using System;

namespace AppConsole.Buisness.Exceptions
{
    public class InvalidCapitalException: Exception
    {
        public InvalidCapitalException() { }
        public InvalidCapitalException(string message) : base(message) { }
    }
}
