﻿using AppConsole.Buisness.Data;
using AppConsole.Buisness.Enums;
using System;
using System.Collections.Generic;

namespace AppConsole
{
    public abstract class DataLoader
    {
        public double getNominalRate(ELoan eLoan, Duration duration)
        {
            double rateOfTheTypeAndDuration = 0.0;

            foreach (ModelRateDTO rate in this.GetData())
            {
                if (rate.TypeLoan.ToUpper() == eLoan.ToString() && rate.Duration == duration)
                {
                    rateOfTheTypeAndDuration = rate.Rate;
                }
            }

            return rateOfTheTypeAndDuration;
        }

        protected abstract List<ModelRateDTO> GetData();
    }
}