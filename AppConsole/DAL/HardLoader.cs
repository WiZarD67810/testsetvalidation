﻿using AppConsole.Buisness.Data;
using System;
using System.Collections.Generic;

namespace AppConsole.DAL
{
    public class HardLoader : DataLoader
    {
        protected override List<ModelRateDTO> GetData()
        {
            List<ModelRateDTO> data = new List<ModelRateDTO>();

            data.Add(new ModelRateDTO("good", 7, 0.62));
            data.Add(new ModelRateDTO("good", 10, 0.67));
            data.Add(new ModelRateDTO("good", 15, 0.85));
            data.Add(new ModelRateDTO("good", 20, 1.04));
            data.Add(new ModelRateDTO("good", 25, 1.27));
            data.Add(new ModelRateDTO("very_good", 7, 0.43));
            data.Add(new ModelRateDTO("very_good", 10, 0.55));
            data.Add(new ModelRateDTO("very_good", 15, 0.73));
            data.Add(new ModelRateDTO("very_good", 20, 0.91));
            data.Add(new ModelRateDTO("very_good", 25, 1.15));
            data.Add(new ModelRateDTO("excellent", 7, 0.35));
            data.Add(new ModelRateDTO("excellent", 10, 0.45));
            data.Add(new ModelRateDTO("excellent", 15, 0.58));
            data.Add(new ModelRateDTO("excellent", 20, 0.73));
            data.Add(new ModelRateDTO("excellent", 25, 0.89));

            return data;

        }
    }
}
